"use strict";

// /* global Carousel JS */

(function ($) {
  function moveCaptions($carousel) {
    var captionElement = $carousel.find(".carousel-item.active .carousel-caption").clone();
    if (captionElement) {
      $carousel.find(".carousel-captions").html(captionElement);
    }
  }

  $.each($(".carousel"), function () {
    var $carousel = $(this);
    var totalItems = $carousel.find(".carousel-item").length;
    var currentIndex = $carousel.find(".carousel-item.active").index() + 1;
    $carousel.find(".carousel-counter").html(currentIndex + "/" + totalItems);
    moveCaptions($carousel);

    $carousel.on("slid.bs.carousel", function () {
      // captions
      moveCaptions($carousel);

      // counter
      currentIndex = $carousel.find(".carousel-item.active").index() + 1;
      $carousel.find(".carousel-counter").html(currentIndex + "/" + totalItems);
    });
  });
})(jQuery);