"use strict";

// /* global Header JS */

(function ($) {
  var headers = $(".layout-content .zpp__component--header");

  // We expect one per page, but this allows more than one
  headers.each(function (i, el) {
    var thisHeader = $(el);

    // Find this header's row parent
    var thisRow = thisHeader.closest(".zpp__row");
    // Figure out how far away from the top of the document this header is
    var thisOffsetTop = thisRow.offset().top;

    // Add classes to the row for CSS to provide sticky styles
    thisRow.addClass("js-header").addClass("js-nav-down");

    // Has the user scrolled yet, and we have recorded it?
    var didScroll = false;
    // What was the window.scrollTop value the last time the user scrolled?
    var lastScrollTop = 0;
    // What is a reasonable offset to hide the navbar?
    var delta = $(window).innerHeight();

    function hasScrolled() {
      var st = $(window).scrollTop();

      // If they scrolled down and are past the navbar position, add class .js-nav-up
      if (st > lastScrollTop && st > thisOffsetTop + delta) {
        // Scrolling Down, animate navbar up to hide
        thisRow.removeClass("js-nav-down").addClass("js-nav-up");
      } else if (st < lastScrollTop) {
        // Scrolling Up, bring navbar down to show
        thisRow.removeClass("js-nav-up").addClass("js-nav-down");
      }
      // Store the value of scrollTop to check against next time
      lastScrollTop = st;
    }

    $(window).scroll(function (event) {
      didScroll = true;
    });

    setInterval(function () {
      if (didScroll) {
        hasScrolled();
        didScroll = false;
      }
    }, 250);
  });

  // find each of the google fonts that we are loading and move them to the <head>
  // as they are not allowed inside the <body>
  $("div > [href^='https://fonts.googleapis.com']").each(function () {
    $(this).detach().appendTo("head");
  });
})(jQuery);