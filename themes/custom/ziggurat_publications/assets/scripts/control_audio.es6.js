/* global moment */

($ => {
  // Get all Audio elements on a page
  const audios = $(".zpp-audio");
  audios.each(function() {
    // Grab the audio element and buttons
    const container = $(this);
    const track = $(this).find("audio");
    const control = $(this).find(".js-audio-control");
    const trackBack = $(this).find(".js-audio-back");
    const trackForward = $(this).find(".js-audio-forward");
    const elapsedContainer = $(this).find(".zpp-audio__track__elapsed");
    const lengthContainer = $(this).find(".zpp-audio__track__length");
    const progress = $(this).find(".zpp-audio__track__progress__bar");

    if (track.length) {
      const i = setInterval(() => {
        // loadedmetadata and durationchange do not seem to be reliable. Using readyState instead
        if (track.get(0).readyState > 0) {
          // Duration should now be available
          const trackSeconds = track.get(0).duration;

          // Convert seconds to utc time so that moment can handle more then 60 seconds
          const trackTime = moment.utc(trackSeconds * 1000).format("m:ss");

          // Put the minutes and seconds in the display
          lengthContainer.text(trackTime);
          clearInterval(i);
        }
      });

      // Remove native controls
      track.attr("controls", false);

      // Listen for a click on the Control button
      control.on("click", () => {
        if (track.get(0).paused) {
          track.get(0).play();
          container.addClass("zpp-audio--playing");
        } else {
          track.get(0).pause();
          container.removeClass("zpp-audio--playing");
        }
      });

      // Listen for a click on the back button
      trackBack.on("click", () => {
        const trackCurrent = track.get(0).currentTime;
        track.get(0).currentTime = trackCurrent - 15;
      });

      trackForward.on("click", () => {
        const trackCurrent = track.get(0).currentTime;
        track.get(0).currentTime = trackCurrent + 30;
      });

      // Update the progress bar width based on elapsed time
      // Update the elapsed time container
      elapsedContainer.text("0.00");
      track.get(0).ontimeupdate = function() {
        const trackCurrent = track.get(0).currentTime;

        progress.css(
          "width",
          `${(trackCurrent / track.get(0).duration) * 100}%`
        );
        // While playing, the duration container becomes an elapsed container
        const trackCurrentTime = moment.utc(trackCurrent * 1000).format("m:ss");
        elapsedContainer.text(trackCurrentTime);
      };

      // If ended, remove the playing class and reset the progress bar
      track.on("ended", () => {
        elapsedContainer.text("0.00");
        container.removeClass("zpp-audio--playing");
        progress.css("width", "0%");
      });
    } else {
      // This is a .zpp-audio wrapper with no <video> element inside. Mark it.
      container.addClass("zpp-audio--empty");
    } // end if
  });
})(jQuery);
