// /* global Modal JS */

// Not a controller. We are using BS4 modals for that
// This adds an extra class to its parent row wrapper

($ => {
  const modals = $(".layout-content .modal");

  modals.each((i, el) => {
    // Add a class to the row parent
    $(el)
      .closest(".zpp__row")
      .addClass("js-modal");
  });
})(jQuery);
