"use strict";

/* global ClipboardJS */
/* global tlite */

(function ($) {
  var clipboard = new ClipboardJS(".js-copy-clipboard");

  clipboard.on("success", function (e) {
    // Generate random class and add to button
    var targetButton = $(e.trigger);
    var randomNum = Math.floor(Math.random() * 26) + Date.now();
    var randomClass = "link-" + randomNum;

    targetButton.addClass(randomClass);

    // Show the tooltip
    tlite.show(document.querySelector("." + randomClass), { grav: "s" });

    // Hide the tooltip
    setTimeout(function () {
      tlite.hide(document.querySelector("." + randomClass));
      targetButton.removeClass(randomClass);
    }, 2000);

    e.clearSelection();
  });
})(jQuery);