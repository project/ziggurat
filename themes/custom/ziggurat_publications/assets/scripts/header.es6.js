// /* global Header JS */

($ => {
  const headers = $(".layout-content .zpp__component--header");

  // We expect one per page, but this allows more than one
  headers.each((i, el) => {
    const thisHeader = $(el);

    // Find this header's row parent
    const thisRow = thisHeader.closest(".zpp__row");
    // Figure out how far away from the top of the document this header is
    const thisOffsetTop = thisRow.offset().top;

    // Add classes to the row for CSS to provide sticky styles
    thisRow.addClass("js-header").addClass("js-nav-down");

    // Has the user scrolled yet, and we have recorded it?
    let didScroll = false;
    // What was the window.scrollTop value the last time the user scrolled?
    let lastScrollTop = 0;
    // What is a reasonable offset to hide the navbar?
    const delta = $(window).innerHeight();

    function hasScrolled() {
      const st = $(window).scrollTop();

      // If they scrolled down and are past the navbar position, add class .js-nav-up
      if (st > lastScrollTop && st > thisOffsetTop + delta) {
        // Scrolling Down, animate navbar up to hide
        thisRow.removeClass("js-nav-down").addClass("js-nav-up");
      } else if (st < lastScrollTop) {
        // Scrolling Up, bring navbar down to show
        thisRow.removeClass("js-nav-up").addClass("js-nav-down");
      }
      // Store the value of scrollTop to check against next time
      lastScrollTop = st;
    }

    $(window).scroll(event => {
      didScroll = true;
    });

    setInterval(() => {
      if (didScroll) {
        hasScrolled();
        didScroll = false;
      }
    }, 250);
  });

  // find each of the google fonts that we are loading and move them to the <head>
  // as they are not allowed inside the <body>
  $("div > [href^='https://fonts.googleapis.com']").each(function() {
    $(this)
      .detach()
      .appendTo("head");
  });
})(jQuery);
