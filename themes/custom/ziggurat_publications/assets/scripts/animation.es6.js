($ => {
  Drupal.behaviors.zigguratPublicationsAnimation = {
    attach(context) {
      // Watch elements onscroll and trigger some events
      // http://codetheory.in/change-active-state-links-sticky-navigation-scroll/
      const animated = $(".layout-content .zpp__animation--initial", context);
      const allRowsLast = $(".node.node--view-mode-full .zpp__row").last();
      const bodyHeight = $("body").prop("scrollHeight");
      const progressBar = $(".layout-content .zpp__component--header");
      let scrollTimeout;

      const scrollEvents = function() {
        const scrollPosition = $(this).scrollTop(); // dynamic value
        const vpHeight = $(window).height(); // static value

        // Trigger animations when blocks come into the viewport
        animated.each((a, el) => {
          const element = $(el);
          const elemOffset = element.offset(); // distance from the top of the document
          const elemOffsetTop = elemOffset.top;
          const elemHeight = element.outerHeight();

          // Subtracting some from topOffset because otherwise, the element animates
          // too late in the scroll, resulting in something that feels laggy
          const topOffset = elemOffsetTop - vpHeight * 0.75;

          if (element.hasClass("animate__sl__fadein")) {
            // The Scroll Length Fade animation triggers on every scroll position change

            let elemCalc = "";
            const elemParentRow = element.parent().parent();

            // The magic:
            // elemCalc = (vpHeight + scrollPosition - elemOffsetTop) / (vpHeight * 0.75);
            // vpHeight is the height dimension of the viewport.
            // scrollposition is how far the page has scrolled measured from the top of the viewport - this is dynamic.
            // elemOffsetTop is the distance from the top of the document to the top of the element.
            // The result of this calculation is essentially what percent of the element is visible in the viewport.

            // Determine if this element matches the LAST one on the page
            // AND is also shorter than the viewport (if not shorter, don't treat it special)
            if (allRowsLast.is(elemParentRow && elemHeight < vpHeight)) {
              // OK! The last element in the Layout Builder stack has this Animation class
              // It is last, so the calculations are different
              // We are subtracting the element height from the viewport height and then that value
              // is being subtracted from the element offset top. This results in "faking" the position
              // of the element to the top of the viewport instead of the bottom.
              elemCalc =
                (vpHeight +
                  scrollPosition -
                  (elemOffsetTop - (vpHeight - elemHeight))) /
                vpHeight;
            } else {
              // This element is somewhere else in the page flow, and we calculate based on its top offset
              // Without the 0.75, we get less than 0 when the element is below the bottom of the viewport
              // and 1.0 when the top of the element hits the top of the viewport.
              // which makes sense - 0% of the element is in view vs. 100% of the element is in view.
              // Multiplying by 0.75 means that when the top of the element is at 75%, it is now 1.0 opacity.
              // It in effect shortens the viewport height by 75%.
              elemCalc =
                (vpHeight + scrollPosition - elemOffsetTop) / (vpHeight * 0.75);
            }

            const elemParent = element.parent();
            elemParent.css({ opacity: elemCalc });

            // Set the values to acceptable CSS integers when it is over/under
            if (elemCalc > "1") {
              elemParent.css({ opacity: 1 });
            } else if (elemCalc < "0") {
              elemParent.css({ opacity: 0 });
            }
          } else if (scrollPosition >= topOffset) {
            // Every other animation effect triggers once

            element.addClass("zpp__animation--active");
          }
        });

        /*
        // Trigger video autoplay when they enter the viewport
        const videos = $(".zpp-video");
        videos.each((v, el) => {
          // Grab the video element and buttons
          const container = $(el);
          const video = container.find("video");
          const control = container.find(".js-video-control");
          if (video.length) {
            // If the videos have controls, manipulate autoplay settings
            if (control.length) {
              const videoHeight = video.innerHeight();
              const videoElemOffset = video.offset(); // distance from the top and left of the document
              const videoElemOffsetTop = videoElemOffset.top;
              // Video offset top plus the video height should be the bottom edge of the video
              const videoScrollTo = videoElemOffsetTop + videoHeight;

              // If a user has interacted with this video, don't change their interaction
              // I.E. play a video on scroll that they have already paused
              if (!container.hasClass("user-initiated")) {
                // Calc = current scroll position (top of viewport) + viewport height - the position that we need to scroll to
                // Essentially, this is the position of the bottom of the video in relation to the bottom of the viewport
                const videoCalc = scrollPosition + vpHeight - videoScrollTo;

                if (videoCalc >= 0 && videoCalc < vpHeight) {
                  // Calc is greater than zero (not a negative number, so its in the viewport)
                  // It is also less than the height of the viewport, so it has not left the top of the viewport
                  container.addClass("zpp-video--playing");
                  container.removeClass(
                    "zpp-video--static zpp-video--paused zpp-video--ended"
                  );
                  video.get(0).play();
                } else {
                  container.removeClass("zpp-video--playing");
                  container.addClass("zpp-video--paused");
                  video.get(0).pause();
                }
              }
            }
          }
        });
        */

        // Change the width of the reader progress bar
        let progressBarWidth = ((scrollPosition + vpHeight) / bodyHeight) * 100;
        if (progressBarWidth > 100) {
          progressBarWidth = 100;
        }
        progressBar.attr("style", `--page-traversed: ${progressBarWidth}%`);
      }; // end scrollEvents

      $(window).on("scroll", () => {
        if (scrollTimeout) {
          clearTimeout(scrollTimeout); // clear the timeout, if one is pending
          scrollTimeout = null;
        }
        // 10ms seems low, but I used a value of 50 and the animations were noticeably janky
        scrollTimeout = setTimeout(scrollEvents, 10);
      });
    }
  };
})(jQuery);
