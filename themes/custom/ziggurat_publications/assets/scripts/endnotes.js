"use strict";

/**
 * @file
 * Endnotes front-end behavior.
 */

(function ($) {
  // Store the height of the headers, since that might be in the way of our scrolling
  var headerHeight = $(".js-header").outerHeight();
  var adminToolbarHeight = $("body").hasClass("adminimal-admin-toolbar toolbar-horizontal") ? 39 : 0;
  var adminTrayHeight = $("body").hasClass("toolbar-tray-open toolbar-horizontal") ? 42 : 0;
  var allHeadersHeight = parseInt(headerHeight + adminToolbarHeight + adminTrayHeight, 10);
  // console.log(`headerHeight = ${headerHeight}`);
  // console.log(`adminToolbarHeight = ${adminToolbarHeight}`);
  // console.log(`adminTrayHeight = ${adminTrayHeight}`);
  // console.log(`allHeadersHeight = ${allHeadersHeight}`);

  // Find all the Endnote Number Anchor Links within the content.
  // <a class="endnote-ref" href="#">1</a>
  var endnotes = $("a.endnote-ref");
  var increment1 = 0;
  endnotes.each(function () {
    increment1 += 1;
    var thisOffTop = $(this).offset().top;
    $(this).prop({
      href: "#endnote-list__link--" + increment1,
      id: "endnote-number__link--" + increment1
    }).attr("data-offset", thisOffTop - allHeadersHeight) // 1
    .addClass("endnote-number__link");
  });
  // 1. Store this element's offset now because sticky divs make incorrect offset measurements once the page is scrolled
  // In the end the markup should look like this:
  // <a class="endnote-ref endnote-number__link" href="#endnote-list__link--1" id="endnote-number__link--1" data-offset="2279">1</a>

  // Modify the Endnote List.
  // <ol class="endnote-list">
  //	<li>Trevor Wyatt Moree, “The Midnight Snack of Andy Warhol”...</li>
  var endnoteList = $(".endnote-list");
  endnoteList.each(function () {
    // Grab the elements inside the Endnote List.
    var listItems = $(this).find("li");

    // Loop through all LIs
    var increment2 = 0;
    listItems.each(function () {
      increment2 += 1;
      var thisOffTop = $(this).offset().top;
      // Create a new anchor inside
      var link = "<a href=\"#endnote-number__link--" + increment2 + "\" class=\"endnote-list__link endnote-list__link--" + increment2 + "\">" + increment2 + "</a>";
      $(this).prop({
        id: "endnote-list__link--" + increment2
      }).attr("data-offset", thisOffTop - allHeadersHeight).addClass("endnote-list__item").prepend(link);
    });
  });
  // In the end the markup should look like this:
  // <li id="endnote-list__link--2" data-offset="4474.390625" class="endnote-list__item"><a href="#endnote-number__link--2" class="endnote-list__link endnote-list__link--2">2</a>Ingrid Shafner, Raid the Icebox Now ...

  // Click Events
  // Handle the click events on endnote references.
  // Clicks on the footnote anchor DOWN to the list item
  $(document).on("click", ".endnote-number__link", function (e) {
    e.preventDefault();

    // Create a jQuery object from the ID referenced in the href
    var destinationId = $(this).attr("href");
    var destinationObj = $(destinationId);

    // console.log(`footnote destinationId = ${destinationId}`);
    // console.log(`footnote destinationObj.length = ${destinationObj.length}`);

    if (destinationObj.length === 1) {
      // Clear any previously added classes
      endnoteList.children().removeClass("target");
      endnotes.removeClass("target");

      // Get the offset of the destination we stored in a data attribute
      var destinationObjOfftop = destinationObj.attr("data-offset");
      // console.log(`footnote destinationObjOfftop = ${destinationObjOfftop}`);

      // Scroll to the destination
      window.scroll(0, destinationObjOfftop, "smooth");

      // Handle the :target because we removed the default click event
      destinationObj.addClass("target").children(".endnote-list__link").focus();
    }
  });

  // Clicks on the link in the Endnote list back UP to the source anchor
  $(document).on("click", ".endnote-list__link", function (e) {
    e.preventDefault();

    // Create a jQuery object from the ID referenced in the href
    var destinationId = $(this).attr("href");
    var destinationObj = $(destinationId);

    // console.log(`anchor destinationId = ${destinationId}`);
    // console.log(`anchor destinationObj.length = ${destinationObj.length}`);

    if (destinationObj.length === 1) {
      // Clear any previously added classes
      endnoteList.children().removeClass("target");
      endnotes.removeClass("target");

      // Get the offset of the destination we stored in a data attribute
      var destinationObjOfftop = destinationObj.attr("data-offset");
      // console.log(`anchor destinationObjOfftop = ${destinationObjOfftop}`);

      // Scroll to the destination
      window.scroll(0, destinationObjOfftop, "smooth");

      // Handle the :target because we removed the default click event
      destinationObj.addClass("target").focus();
    }
  });
})(jQuery);