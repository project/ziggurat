// /* global Carousel JS */

($ => {
  function moveCaptions($carousel) {
    const captionElement = $carousel
      .find(".carousel-item.active .carousel-caption")
      .clone();
    if (captionElement) {
      $carousel.find(".carousel-captions").html(captionElement);
    }
  }

  $.each($(".carousel"), function() {
    const $carousel = $(this);
    const totalItems = $carousel.find(".carousel-item").length;
    let currentIndex = $carousel.find(".carousel-item.active").index() + 1;
    $carousel.find(".carousel-counter").html(`${currentIndex}/${totalItems}`);
    moveCaptions($carousel);

    $carousel.on("slid.bs.carousel", () => {
      // captions
      moveCaptions($carousel);

      // counter
      currentIndex = $carousel.find(".carousel-item.active").index() + 1;
      $carousel.find(".carousel-counter").html(`${currentIndex}/${totalItems}`);
    });
  });
})(jQuery);
