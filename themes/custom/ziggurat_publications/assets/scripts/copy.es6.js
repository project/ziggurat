/* global ClipboardJS */
/* global tlite */

($ => {
  const clipboard = new ClipboardJS(".js-copy-clipboard");

  clipboard.on("success", e => {
    // Generate random class and add to button
    const targetButton = $(e.trigger);
    const randomNum = Math.floor(Math.random() * 26) + Date.now();
    const randomClass = `link-${randomNum}`;

    targetButton.addClass(randomClass);

    // Show the tooltip
    tlite.show(document.querySelector(`.${randomClass}`), { grav: "s" });

    // Hide the tooltip
    setTimeout(() => {
      tlite.hide(document.querySelector(`.${randomClass}`));
      targetButton.removeClass(randomClass);
    }, 2000);

    e.clearSelection();
  });
})(jQuery);
