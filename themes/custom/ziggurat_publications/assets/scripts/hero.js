"use strict";

// /* global Hero JS */

(function ($) {
  // The thing we are solving:
  // Mobile devices, mostly, but tablet too: CSS 100vh is larger than the viewport
  // because of the browser bar or OS chrome. Only when scrolled and the browser bar
  // goes away is 100vh actually 100vh.
  // This trick calculates the actual viewport with JS and sets a custom property
  // that our CSS watches.
  // https://css-tricks.com/the-trick-to-viewport-units-on-mobile/

  window.addEventListener("resize", function () {
    // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
    var vh = window.innerHeight * 0.01;
    // Then we set the value in the --vh custom property to the root of the document
    document.documentElement.style.setProperty("--vh", vh + "px");
  });
})(jQuery);