"use strict";

/* global moment */

(function ($) {
  // Some good code here for useful inspiration
  // https://github.com/iandevlin/iandevlin.github.io/blob/master/mdn/video-player/js/video-player.js
  // https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Client-side_web_APIs/Video_and_audio_APIs

  // Does the browser actually support the video element?
  var supportsVideo = !!document.createElement("video").canPlayType;

  function toggleFullScreen(element) {
    if (!element.fullscreenElement || !element.mozFullscreenElement || !element.msFullscreenElement || !element.webkitFullscreenElement) {
      if (element.requestFullscreen) {
        element.requestFullscreen();
      } else if (element.mozRequestFullScreen) {
        element.mozRequestFullScreen();
        element.attr("controls", true);
      } else if (element.webkitRequestFullscreen) {
        element.webkitRequestFullscreen();
      } else if (element.msRequestFullscreen) {
        element.msRequestFullscreen();
      }
    } else if (document.exitFullscreen) {
      document.exitFullscreen();
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen();
      element.attr("controls", false);
    } else if (document.webkitExitFullscreen) {
      document.webkitExitFullscreen();
    }
  }

  function playmedia(track, container) {
    if (track.get(0).paused) {
      track.get(0).play();
      // We use the `user-initiated` class to tell our animation JS that a user intentionally paused/played a track
      // Video paused/playing classes are self explanatory, and show/hide controls or swap icons
      // Video "static" is meant to only be on the wrapper the first time a user initiates play
      //   It controls the large play icon that should go away for subsequent interactions
      container.addClass("zpp-video--playing user-initiated");
      container.removeClass("zpp-video--static zpp-video--paused");
    } else {
      track.get(0).pause();
      container.removeClass("zpp-video--playing");
      container.addClass("zpp-video--paused user-initiated");
    }
  }

  // Don't proceed unless video is supported
  if (supportsVideo) {
    // Get all Video elements on a page
    var videos = $(".zpp-video");
    videos.each(function () {
      // Grab the video element and buttons
      var container = $(this);
      var track = container.find("video");
      var initial = container.find(".zpp-video__track__play");
      var control = container.find(".js-video-control");
      var fullscreen = container.find(".js-fs-control");
      var durationContainer = container.find(".zpp-video__track__duration");
      var progress = container.find(".zpp-video__track__progress__bar");
      var poster = container.find(".zpp-video__poster");
      var trackTime = "";

      if (track.length) {
        // If videos have controls
        if (control.length) {
          // Remove native controls
          track.attr("controls", false);

          // Get the time of the video track
          var i = setInterval(function () {
            // loadedmetadata and durationchange do not seem to be reliable. Using readyState instead
            if (track.get(0).readyState > 0) {
              // Duration should now be available
              var trackSeconds = track.get(0).duration;

              // Convert seconds to utc time so that moment can handle more then 60 seconds
              trackTime = moment.utc(trackSeconds * 1000).format("HH:mm:ss");

              // Put the minutes and seconds in the display
              durationContainer.text(trackTime);
              clearInterval(i);
            }
          });

          // Initial load has a big play button visible
          // SVG fades out (opacity) but button stays in place as large control target
          initial.on("click", function () {
            playmedia(track, container);
          });

          // Listen for a click on the Control button
          control.on("click", function () {
            playmedia(track, container);
          });

          // Update the progress bar width based on elapsed time
          track.get(0).ontimeupdate = function () {
            var trackCurrent = track.get(0).currentTime;
            progress.css("width", trackCurrent / track.get(0).duration * 100 + "%");
            // While playing, the duration container becomes an elapsed container
            var currentTime = moment.utc(trackCurrent * 1000).format("HH:mm:ss");
            durationContainer.text(currentTime);
          };

          // If ended, remove the playing class and reset the progress bar
          track.on("ended", function () {
            container.removeClass("zpp-video--playing");
            container.addClass("zpp-video--static");
            progress.css("width", "0%");
          });

          // Fullscreen
          fullscreen.on("click", function () {
            toggleFullScreen(track.get(0));
          });

          // Set poster on video tag
          if (poster.length) {
            track.attr("poster", poster.data("poster-src"));
          }
        } else {
          // If video doesn't have controls
          // Set poster on video tag
          track.attr("poster", poster.data("poster-src"));
        }
        // PURPOSELY COMMENTING THIS OUT
        // WITH NEW CODE WE ARE GETTING FALSE POSITIVES
        // AND THIS MIGHT NOT BE NEEDED ANYMORE
        // } else {
        //   // This is a .zpp-video wrapper with no <video> element inside. Mark it.
        //   container.addClass("zpp-video--empty");
      } // end if (true) {}
    });
  }
})(jQuery);