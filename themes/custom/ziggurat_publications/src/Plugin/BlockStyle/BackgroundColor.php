<?php

namespace Drupal\ziggurat_publications\Plugin\BlockStyle;

use Drupal\Core\Form\FormStateInterface;
use Drupal\block_style_plugins\Plugin\BlockStyleBase;

/**
 * Provides a 'BackgroundColor' block style.
 *
 * This block style is for adding a background color in a select list.
 *
 * @BlockStyle(
 *  id = "bg_color_class",
 *  label = @Translation("Background Color"),
 * )
 */
class BackgroundColor extends BlockStyleBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['bg_color_class' => 'zpp__bg-color--transparent'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // The value of the options should be the class name which will be applied.
    $elements['bg_color_class'] = [
      '#type' => 'select',
      '#title' => $this->t('Component Background Color'),
      '#options' => [
        'zpp__bg-color--transparent' => $this->t('None'),
        'zpp__bg-color--default-light' => $this->t('Default Light'),
        'zpp__bg-color--default-dark' => $this->t('Default Dark'),
        'zpp__bg-color--primary' => $this->t('Primary'),
        'zpp__bg-color--color-one' => $this->t('Color 1'),
        'zpp__bg-color--color-two' => $this->t('Color 2'),
        'zpp__bg-color--color-three' => $this->t('Color 3'),
        'zpp__bg-color--color-four' => $this->t('Color 4'),
        'zpp__bg-color--color-five' => $this->t('Color 5'),
        'zpp__bg-color--color-six' => $this->t('Color 6'),
        'zpp__bg-color--color-seven' => $this->t('Color 7'),
        'zpp__bg-color--color-eight' => $this->t('Color 8'),
        'zpp__bg-color--color-nine' => $this->t('Color 9'),
        'zpp__bg-color--color-ten' => $this->t('Color 10'),
      ],
      '#default_value' => $this->configuration['bg_color_class'],
    ];

    return $elements;

  }

}
