<?php

namespace Drupal\ziggurat_publications\Plugin\BlockStyle;

use Drupal\Core\Form\FormStateInterface;
use Drupal\block_style_plugins\Plugin\BlockStyleBase;

/**
 * Provides a 'CustomClass' block style.
 *
 * This block style is for adding a class in a text field.
 *
 * @BlockStyle(
 *  id = "custom_class",
 *  label = @Translation("Custom Class"),
 * )
 */
class CustomClass extends BlockStyleBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['custom_class' => ''];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $elements['custom_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Component Custom CSS Class(es)'),
      '#placeholder' => $this->t('pb-md-5 p-lg-2'),
      '#description' => $this->t('Do not add the "period" to the start of the class'),
      '#default_value' => $this->configuration['custom_class'],
    ];

    return $elements;

  }

}
