<?php

namespace Drupal\ziggurat_publications\Plugin\BlockStyle;

use Drupal\Core\Form\FormStateInterface;
use Drupal\block_style_plugins\Plugin\BlockStyleBase;

/**
 * Provides a 'Animation' block style.
 *
 * This block style is for adding an animation option in a select list.
 *
 * @BlockStyle(
 *  id = "animation_class",
 *  label = @Translation("Animation"),
 * )
 */
class Animation extends BlockStyleBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['animation_class' => 'zpp__animation--off'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // The value of the options should be the class name which will be applied.
    $elements['animation_class'] = [
      '#type' => 'select',
      '#title' => $this->t('Component Animation'),
      '#options' => [
        'zpp__animation--off' => $this->t('No Animation'),
        'zpp__animation--initial animate__sl__fadein' => $this->t('Scroll Fade In'),
        'zpp__animation--initial animate__once__fadein' => $this->t('Fade In'),
        'zpp__animation--initial animate__once__fadeinleft' => $this->t('Fade In Left'),
        'zpp__animation--initial animate__once__fadeinright' => $this->t('Fade In Right'),
        'zpp__animation--initial animate__once__fadeinup' => $this->t('Fade In Up'),
        'zpp__animation--initial animate__once__slideinleft' => $this->t('Slide In Left'),
        'zpp__animation--initial animate__once__slideinright' => $this->t('Slide In Right'),
        'zpp__animation--initial animate__once__slideinup' => $this->t('Slide In Up'),
        'zpp__animation--initial animate__once__zoomin' => $this->t('Zoom In'),
        'zpp__animation--initial animate__once__zoominleft' => $this->t('Zoom In Left'),
        'zpp__animation--initial animate__once__zoominright' => $this->t('Zoom In Right'),
        'zpp__animation--initial animate__once__zoominup' => $this->t('Zoom In Up'),
      ],
      '#default_value' => $this->configuration['animation_class'],
    ];

    return $elements;

  }

}
