<?php

namespace Drupal\ziggurat_publications\Plugin\BlockStyle;

use Drupal\Core\Form\FormStateInterface;
use Drupal\block_style_plugins\Plugin\BlockStyleBase;

/**
 * Provides a 'Spacing' block style.
 *
 * This block style is for adding a Spacing option in a select list.
 *
 * @BlockStyle(
 *  id = "spacing_class",
 *  label = @Translation("Spacing"),
 * )
 */
class Spacing extends BlockStyleBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['spacing_class' => 'zpp__spacing--none'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // The value of the options should be the class name which will be applied.
    $elements['spacing_class'] = [
      '#type' => 'select',
      '#title' => $this->t('Component Spacing'),
      '#options' => [
        'zpp__spacing--none' => $this->t('None'),
        'zpp__spacing--small' => $this->t('Small'),
        'zpp__spacing--normal' => $this->t('Normal'),
        'zpp__spacing--large' => $this->t('Large'),
        'zpp__spacing--xlarge' => $this->t('Extra Large'),
      ],
      '#default_value' => $this->configuration['spacing_class'],
    ];

    return $elements;

  }

}
