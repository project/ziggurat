<?php

namespace Drupal\ziggurat_publications\Plugin\BlockStyle;

use Drupal\Core\Form\FormStateInterface;
use Drupal\block_style_plugins\Plugin\BlockStyleBase;

/**
 * Provides a 'MediaPlayerTheme' block style.
 *
 * This block style is for adding a Media Player Theme in a select list.
 *
 * @BlockStyle(
 *  id = "media_player_theme",
 *  label = @Translation("Media Player Theme"),
 * )
 */
class MediaPlayerTheme extends BlockStyleBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['media_player_theme' => 'zpp-audio__theme--light'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // The value of the options should be the class name which will be applied.
    $elements['media_player_theme'] = [
      '#type' => 'select',
      '#title' => $this->t('Component Media Player Theme'),
      '#options' => [
        'zpp-audio__theme--light' => $this->t('Light Player Theme'),
        'zpp-audio__theme--dark' => $this->t('Dark Player Theme'),
      ],
      '#default_value' => $this->configuration['media_player_theme'],
    ];

    return $elements;

  }

}
