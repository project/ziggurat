<?php

namespace Drupal\ziggurat_publications\Plugin\BlockStyle;

use Drupal\Core\Form\FormStateInterface;
use Drupal\block_style_plugins\Plugin\BlockStyleBase;

/**
 * Provides a 'Zoom' block style.
 *
 * This block style is for adding a Zoom option in a select list.
 *
 * @BlockStyle(
 *  id = "zoom_class",
 *  label = @Translation("Zoom"),
 * )
 */
class Zoom extends BlockStyleBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['zoom_class' => 'zpp__zoom--off'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // The value of the options should be the class name which will be applied.
    $elements['zoom_class'] = [
      '#type' => 'select',
      '#title' => $this->t('Component Zoom'),
      '#options' => [
        'zpp__zoom--off' => $this->t('Off'),
        'zpp__zoom--on' => $this->t('On'),
      ],
      '#default_value' => $this->configuration['zoom_class'],
    ];

    return $elements;

  }

}
