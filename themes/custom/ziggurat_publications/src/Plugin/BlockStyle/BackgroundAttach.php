<?php

namespace Drupal\ziggurat_publications\Plugin\BlockStyle;

use Drupal\Core\Form\FormStateInterface;
use Drupal\block_style_plugins\Plugin\BlockStyleBase;

/**
 * Provides a 'BackgroundAttach' block style.
 *
 * This block style is for adding a background attach option in a select list.
 *
 * @BlockStyle(
 *  id = "bg_attach_class",
 *  label = @Translation("Background Attach"),
 * )
 */
class BackgroundAttach extends BlockStyleBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['bg_attach_class' => 'zpp__bg-attach--none'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // The value of the options should be the class name which will be applied.
    $elements['bg_attach_class'] = [
      '#type' => 'select',
      '#title' => $this->t('Component Background Attach'),
      '#options' => [
        'zpp__bg-attach--none' => $this->t('No Background'),
        'zpp__bg-attach--contain' => $this->t('Contain Background'),
        'zpp__bg-attach--cover' => $this->t('Cover Background'),
      ],
      '#default_value' => $this->configuration['bg_attach_class'],
    ];

    return $elements;

  }

}
