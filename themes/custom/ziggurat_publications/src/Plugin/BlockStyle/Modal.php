<?php

namespace Drupal\ziggurat_publications\Plugin\BlockStyle;

use Drupal\Core\Form\FormStateInterface;
use Drupal\block_style_plugins\Plugin\BlockStyleBase;

/**
 * Provides a 'Modal' block style.
 *
 * This block style is for adding a modal option in a select list.
 *
 * @BlockStyle(
 *  id = "modal_class",
 *  label = @Translation("Modal"),
 * )
 */
class Modal extends BlockStyleBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['modal_class' => 'zpp__modal--off'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // The value of the options should be the class name which will be applied.
    $elements['modal_class'] = [
      '#type' => 'select',
      '#title' => $this->t('Component Modal'),
      '#options' => [
        'zpp__modal--off' => $this->t('Off'),
        'zpp__modal--on' => $this->t('On'),
      ],
      '#default_value' => $this->configuration['modal_class'],
    ];

    return $elements;

  }

}
