<?php

namespace Drupal\ziggurat_publications\Plugin\BlockStyle;

use Drupal\Core\Form\FormStateInterface;
use Drupal\block_style_plugins\Plugin\BlockStyleBase;

/**
 * Provides an 'Alignment' block style.
 *
 * This block style is for adding an Alignment option in a select list.
 *
 * @BlockStyle(
 *  id = "alignment_class",
 *  label = @Translation("Alignment"),
 * )
 */
class Alignment extends BlockStyleBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['alignment_class' => 'zpp__block-alignment zpp__block-alignment--normal'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // The value of the options should be the class name which will be applied.
    $elements['alignment_class'] = [
      '#type' => 'select',
      '#title' => $this->t('Component Alignment'),
      '#options' => [
        'zpp__block-alignment zpp__block-alignment--normal' => $this->t('Align item normal'),
        'zpp__block-alignment zpp__block-alignment--left' => $this->t('Align item left'),
        'zpp__block-alignment zpp__block-alignment--right' => $this->t('Align item right'),
        'zpp__block-alignment zpp__block-alignment--center' => $this->t('Align item center'),
      ],
      '#default_value' => $this->configuration['alignment_class'],
    ];

    return $elements;

  }

}
