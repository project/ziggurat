<?php

namespace Drupal\ziggurat_publications\Plugin\BlockStyle;

use Drupal\Core\Form\FormStateInterface;
use Drupal\block_style_plugins\Plugin\BlockStyleBase;

/**
 * Provides a 'ColumnCount' block style.
 *
 * This block style is for adding a Column Option in a select list.
 *
 * @BlockStyle(
 *  id = "column_count_class",
 *  label = @Translation("Column Count"),
 * )
 */
class ColumnCount extends BlockStyleBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['column_count_class' => 'zpp__column-count--one'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // The value of the options should be the class name which will be applied.
    $elements['column_count_class'] = [
      '#type' => 'select',
      '#title' => $this->t('Component Column Count'),
      '#options' => [
        'zpp__column-count--one' => $this->t('1 Column'),
        'zpp__column-count--two' => $this->t('2 Columns'),
        'zpp__column-count--three' => $this->t('3 Columns'),
        'zpp__column-count--four' => $this->t('4 Columns'),
        'zpp__column-count--five' => $this->t('5 Columns'),
      ],
      '#default_value' => $this->configuration['column_count_class'],
    ];

    return $elements;

  }

}
