<?php

use Drupal\Core\Config\FileStorage;
/**
 * @file
 */

/**
 * Set up install task.
 */
function ziggurat_install_tasks() {

  return [
    'ziggurat_install_default_content' => [
      'display_name' => "Install default ziggurat content",
    ],
  ];
}

/**
 * Enable modules after initial profile install.
 */
function ziggurat_install_default_content() {
  if (\Drupal::moduleHandler()->moduleExists('default_content')) {
    \Drupal::service('module_installer')->install(['ziggurat_default_content']);
    \Drupal::service('module_installer')->install(['nodeaccess']);
    \Drupal::service('module_installer')->install(['ziggurat_extra_config']);

    $config_name = 'nodeaccess.settings';
    $config_path = drupal_get_path('profile', 'ziggurat') . '/config/extra';
    $source = new FileStorage($config_path);
    $config_storage = \Drupal::service('config.storage');
    $config_storage->write($config_name, $source->read($config_name));
  }
}
