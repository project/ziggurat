<?php

declare(strict_types = 1);

namespace Drupal\ziggurat_publications_layout;

/**
 * Provides constants for the Ziggurat Publications Layout module.
 */
final class ZigguratLayout {

  public const ROW_WIDTH_100 = '100';

  public const ROW_WIDTH_75 = '75';

  public const ROW_WIDTH_67 = '67';

  public const ROW_WIDTH_50 = '50';

  public const ROW_WIDTH_33 = '33';

  public const ROW_WIDTH_25 = '25';

  public const ROW_WIDTH_25_75 = '25-75';

  public const ROW_WIDTH_33_67 = '33-67';

  public const ROW_WIDTH_50_50 = '50-50';

  public const ROW_WIDTH_67_33 = '67-33';

  public const ROW_WIDTH_75_25 = '75-25';

  public const ROW_WIDTH_33_33_33 = '33-33-33';

  public const ROW_WIDTH_25_25_50 = '25-25-50';

  public const ROW_WIDTH_25_50_25 = '25-50-25';

  public const ROW_WIDTH_50_25_25 = '50-25-25';

  public const HORIZONTAL_ALIGNMENT_LEFT = 'left';

  public const HORIZONTAL_ALIGNMENT_CENTER = 'center';

  public const HORIZONTAL_ALIGNMENT_RIGHT = 'right';

  public const VERTICAL_ALIGNMENT_TOP = 'top';

  public const VERTICAL_ALIGNMENT_MIDDLE = 'middle';

  public const VERTICAL_ALIGNMENT_BOTTOM = 'bottom';

  public const NO_BACKGROUND_COLOR = 0;

}
