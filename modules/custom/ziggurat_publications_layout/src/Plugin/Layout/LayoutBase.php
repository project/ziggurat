<?php

declare(strict_types = 1);

namespace Drupal\ziggurat_publications_layout\Plugin\Layout;

use Drupal\ziggurat_publications_layout\ZigguratLayout;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\taxonomy\Entity\Term;

/**
 * Provides a layout base for custom layouts.
 */
abstract class LayoutBase extends LayoutDefault implements PluginFormInterface {

  /**
   * {@inheritdoc}
   */
  public function build(array $regions): array {
    $build = parent::build($regions);
    $backgroundColor = $this->configuration['background_color'];
    if ($backgroundColor) {
      $build['#attributes']['class'][] = 'zpp__bg-color--' . $backgroundColor;
    }

    $rowWidth = $this->configuration['row_width'];
    if ($rowWidth) {
      $build['#attributes']['class'][] = 'zpp__row-width--' . $rowWidth;
    }

    $sticky = $this->configuration['sticky'];
    if ($sticky) {
      $build['#attributes']['class'][] = 'zpp__sticky';
    }

    $hero = $this->configuration['hero'];
    if ($hero) {
      $build['#attributes']['class'][] = 'zpp__hero';
    }

    $horizontalAlignment = $this->configuration['horizontal_alignment'];
    if ($horizontalAlignment) {
      $build['#attributes']['class'][] = 'zpp__horizontal-alignment--' . $horizontalAlignment;
    }

    $verticalAlignment = $this->configuration['vertical_alignment'];
    if ($verticalAlignment) {
      $build['#attributes']['class'][] = 'zpp__vertical-alignment--' . $verticalAlignment;
    }

    $class = $this->configuration['class'];
    if ($class) {
      $build['#attributes']['class'] = array_merge(
        explode(' ', $this->configuration['class']),
        $build['#attributes']['class']
      );
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'background_color' => ZigguratLayout::NO_BACKGROUND_COLOR,
      'id' => NULL,
      'class' => NULL,
      'row_width' => $this->getDefaultRowWidth(),
      'sticky' => FALSE,
      'hero' => FALSE,
      'horizontal_alignment' => ZigguratLayout::HORIZONTAL_ALIGNMENT_LEFT,
      'vertical_alignment' => ZigguratLayout::VERTICAL_ALIGNMENT_MIDDLE,
      'primary_webfont_url' => NULL,
      'primary_webfont_family' => NULL,
      'primary_webfont_type' => NULL,
      'primary_system_font' => NULL,
      'secondary_webfont_url' => NULL,
      'secondary_webfont_family' => NULL,
      'secondary_webfont_type' => NULL,
      'secondary_system_font' => NULL,
      'background_color_override' => NULL,
      'foreground_color_override' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $rowWidths = $this->getRowWidths();
    if (!empty($rowWidths)) {
      $form['layout'] = [
        '#type' => 'details',
        '#title' => $this->t('Layout'),
        '#open' => TRUE,
        '#weight' => 30,
      ];

      $form['layout']['row_width'] = [
        '#type' => 'radios',
        '#title' => $this->t('Row Width'),
        '#options' => $rowWidths,
        '#default_value' => $this->configuration['row_width'],
        '#required' => TRUE,
      ];
    }

    $form['alignment'] = [
      '#type' => 'details',
      '#title' => $this->t('Alignment'),
      '#open' => TRUE,
      '#weight' => 10,
    ];

    $form['alignment']['horizontal_alignment'] = [
      '#type' => 'radios',
      '#title' => $this->t('Horizontal Alignment'),
      '#options' => $this->getHorizontalAlignments(),
      '#default_value' => $this->configuration['horizontal_alignment'],
    ];

    $form['alignment']['vertical_alignment'] = [
      '#type' => 'radios',
      '#title' => $this->t('Vertical Alignment'),
      '#options' => $this->getVerticalAlignments(),
      '#default_value' => $this->configuration['vertical_alignment'],
    ];

    $form['background'] = [
      '#type' => 'details',
      '#title' => $this->t('Background'),
      '#open' => TRUE,
      '#weight' => 20,
    ];

    $form['background']['background_color'] = [
      '#type' => 'ziggurat_publications_layout_background_color_radios',
      '#plugin_id' => $this->getPluginDefinition()->id(),
      '#default_value' => $this->configuration['background_color'],
    ];

    $form['background']['overrides'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Overrides'),
    ];

    $form['background']['overrides']['background_color_override'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Background Color'),
      '#default_value' => $this->configuration['background_color_override'],
      '#attributes' => [
        'placeholder' => '#000000',
      ],
    ];

    $form['background']['overrides']['foreground_color_override'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Foreground Color'),
      '#default_value' => $this->configuration['foreground_color_override'],
      '#attributes' => [
        'placeholder' => '#ffffff',
      ],
    ];

    $form['extra'] = [
      '#type' => 'details',
      '#title' => $this->t('Extra'),
      '#open' => TRUE,
      '#weight' => 40,
    ];

    $form['extra']['sticky'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Sticky Mode'),
      'checkbox' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable Sticky Row'),
        '#description' => $this->t('Enabling sticky row will make this section stuck when it scrolls and reaches the top of the browser.'),
        '#default_value' => $this->configuration['sticky'],
      ],
    ];

    $form['extra']['hero'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Hero Mode'),
      'checkbox' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable Hero Row'),
        '#description' => $this->t('Enabling hero row will apply hero styles set in the theme to this section.'),
        '#default_value' => $this->configuration['hero'],
      ],
    ];

    $form['extra']['attributes'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Attributes'),
    ];

    $form['extra']['attributes']['id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom ID'),
      '#description' => $this->t('Enter custom css ID for this row.'),
      '#default_value' => $this->configuration['id'],
      '#attributes' => [
        'placeholder' => 'unique-identifier',
      ],
    ];

    $form['extra']['attributes']['class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom Class'),
      '#description' => $this->t('Enter custom css classes for this row. Separate multiple classes by a space and do not include a period.'),
      '#default_value' => $this->configuration['class'],
      '#attributes' => [
        'placeholder' => 'class-one class-two',
      ],
    ];

    $form['typography'] = [
      '#type' => 'details',
      '#title' => $this->t('Typography'),
      '#open' => TRUE,
      '#weight' => 60,
    ];

    $form['typography']['primary'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Primary Webfont'),
    ];

    $form['typography']['primary']['primary_webfont_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webfont URL'),
      '#default_value' => $this->configuration['primary_webfont_url'],
      '#attributes' => [
        'placeholder' => 'https://fonts.googleapis.com/css2?...',
      ],
    ];

    $form['typography']['primary']['primary_webfont_family'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webfont Family'),
      '#default_value' => $this->configuration['primary_webfont_family'],
      '#attributes' => [
        'placeholder' => 'Roboto Slab',
      ],
    ];

    $form['typography']['primary']['primary_webfont_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webfont Type'),
      '#default_value' => $this->configuration['primary_webfont_type'],
      '#attributes' => [
        'placeholder' => 'serif',
      ],
    ];

    $form['typography']['secondary'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Secondary Webfont'),
    ];

    $form['typography']['secondary']['secondary_webfont_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webfont URL'),
      '#default_value' => $this->configuration['secondary_webfont_url'],
      '#attributes' => [
        'placeholder' => 'https://fonts.googleapis.com/css2?...',
      ],
    ];

    $form['typography']['secondary']['secondary_webfont_family'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webfont Family'),
      '#default_value' => $this->configuration['secondary_webfont_family'],
      '#attributes' => [
        'placeholder' => 'Roboto',
      ],
    ];

    $form['typography']['secondary']['secondary_webfont_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webfont Type'),
      '#default_value' => $this->configuration['secondary_webfont_type'],
      '#attributes' => [
        'placeholder' => 'sans-serif',
      ],
    ];

    $primary_term = '';
    $secondary_term = '';

    if ($this->configuration['primary_system_font']) {
      $primary_term = Term::load($this->configuration['primary_system_font']);
    }

    if ($this->configuration['secondary_system_font']) {
      $secondary_term = Term::load($this->configuration['secondary_system_font']);
    }

    $form['typography']['system'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('System Fonts'),
    ];

    $form['typography']['system']['primary_system_font'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'taxonomy_term',
      '#selection_settings' => [
        'target_bundles' => [
          'system_fonts',
        ],
      ],
      '#default_value' => ($primary_term ? $primary_term : Term::load(9)),
      '#title' => $this->t('Primary System Font'),
    ];

    $form['typography']['system']['secondary_system_font'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'taxonomy_term',
      '#selection_settings' => [
        'target_bundles' => [
          'system_fonts',
        ],
      ],
      '#default_value' => ($secondary_term ? $secondary_term : Term::load(9)),
      '#title' => $this->t('Secondary System Font'),
    ];

    $form['#attached']['library'][] = 'ziggurat_publications_layout/layout_builder';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->configuration['background_color'] = $values['background']['background_color'];
    $this->configuration['class'] = $values['extra']['attributes']['class'];
    $this->configuration['id'] = $values['extra']['attributes']['id'];
    $this->configuration['row_width'] = $values['layout']['row_width'];
    $this->configuration['sticky'] = (bool) $values['extra']['sticky']['checkbox'];
    $this->configuration['hero'] = (bool) $values['extra']['hero']['checkbox'];
    $this->configuration['horizontal_alignment'] = $values['alignment']['horizontal_alignment'];
    $this->configuration['vertical_alignment'] = $values['alignment']['vertical_alignment'];
    $this->configuration['primary_webfont_url'] = $values['typography']['primary']['primary_webfont_url'];
    $this->configuration['primary_webfont_family'] = $values['typography']['primary']['primary_webfont_family'];
    $this->configuration['primary_webfont_type'] = $values['typography']['primary']['primary_webfont_type'];
    $this->configuration['primary_system_font'] = $values['typography']['system']['primary_system_font'];
    $this->configuration['secondary_webfont_url'] = $values['typography']['secondary']['secondary_webfont_url'];
    $this->configuration['secondary_webfont_family'] = $values['typography']['secondary']['secondary_webfont_family'];
    $this->configuration['secondary_webfont_type'] = $values['typography']['secondary']['secondary_webfont_type'];
    $this->configuration['secondary_system_font'] = $values['typography']['system']['secondary_system_font'];
    $this->configuration['background_color_override'] = $values['background']['overrides']['background_color_override'];
    $this->configuration['foreground_color_override'] = $values['background']['overrides']['foreground_color_override'];
  }

  /**
   * Get the row widths.
   *
   * @return array
   *   The row widths.
   */
  abstract protected function getRowWidths(): array;

  /**
   * Get the default row width.
   *
   * @return string
   *   The default row width.
   */
  abstract protected function getDefaultRowWidth(): string;

  /**
   * Get the horizontal alignments.
   *
   * @return array
   *   The horizontal alignments.
   */
  protected function getHorizontalAlignments(): array {
    return [
      ZigguratLayout::HORIZONTAL_ALIGNMENT_LEFT => $this->t('Left'),
      ZigguratLayout::HORIZONTAL_ALIGNMENT_CENTER => $this->t('Center'),
      ZigguratLayout::HORIZONTAL_ALIGNMENT_RIGHT => $this->t('Right'),
    ];
  }

  /**
   * Get the vertical alignments.
   *
   * @return array
   *   The vertical alignments.
   */
  protected function getVerticalAlignments(): array {
    return [
      ZigguratLayout::VERTICAL_ALIGNMENT_TOP => $this->t('Top'),
      ZigguratLayout::VERTICAL_ALIGNMENT_MIDDLE => $this->t('Middle'),
      ZigguratLayout::VERTICAL_ALIGNMENT_BOTTOM => $this->t('Bottom'),
    ];
  }

  /**
   * Determine if this layout has alignment settings.
   *
   * @return bool
   *   If this layout has alignment settings.
   */
  protected function hasAlignmentSettings(): bool {
    return $this->configuration['horizontal_alignment']
      || $this->configuration['vertical_alignment'];
  }

  /**
   * Determine if this layout has background settings.
   *
   * @return bool
   *   If this layout has background settings.
   */
  protected function hasBackgroundSettings(): bool {
    return (bool) $this->configuration['background_color'];
  }

  /**
   * Determine if this layout has extra settings.
   *
   * @return bool
   *   If this layout has extra settings.
   */
  protected function hasExtraSettings(): bool {
    return $this->configuration['class']
      || $this->configuration['sticky']
      || $this->configuration['hero'];

  }

}
