<?php

declare(strict_types = 1);

namespace Drupal\ziggurat_publications_layout\Plugin\Layout;

use Drupal\ziggurat_publications_layout\ZigguratLayout;

/**
 * Provides a plugin class for two column layouts.
 */
final class TwoColumnLayout extends LayoutBase {

  /**
   * {@inheritdoc}
   */
  protected function getRowWidths(): array {
    return [
      ZigguratLayout::ROW_WIDTH_25_75 => $this->t('25% / 75%'),
      ZigguratLayout::ROW_WIDTH_33_67 => $this->t('33% / 67%'),
      ZigguratLayout::ROW_WIDTH_50_50 => $this->t('50% / 50%'),
      ZigguratLayout::ROW_WIDTH_67_33 => $this->t('67% / 33%'),
      ZigguratLayout::ROW_WIDTH_75_25 => $this->t('75% / 25%'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultRowWidth(): string {
    return ZigguratLayout::ROW_WIDTH_50_50;
  }

}
