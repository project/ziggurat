<?php

declare(strict_types = 1);

namespace Drupal\ziggurat_publications_layout\Plugin\Layout;

use Drupal\ziggurat_publications_layout\ZigguratLayout;

/**
 * Provides a plugin class for three column layouts.
 */
final class ThreeColumnLayout extends LayoutBase {

  /**
   * {@inheritdoc}
   */
  protected function getRowWidths(): array {
    return [
      ZigguratLayout::ROW_WIDTH_33_33_33 => $this->t('33% / 33% / 33%'),
      ZigguratLayout::ROW_WIDTH_25_25_50 => $this->t('25% / 25% / 50%'),
      ZigguratLayout::ROW_WIDTH_25_50_25 => $this->t('25% / 50% / 25%'),
      ZigguratLayout::ROW_WIDTH_50_25_25 => $this->t('50% / 25% / 25%'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultRowWidth(): string {
    return ZigguratLayout::ROW_WIDTH_33_33_33;
  }

}
