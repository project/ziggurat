<?php

declare(strict_types = 1);

namespace Drupal\ziggurat_publications_layout\Plugin\Layout;

use Drupal\ziggurat_publications_layout\ZigguratLayout;

/**
 * Provides a plugin class for one column layouts.
 */
final class OneColumnLayout extends LayoutBase {

  /**
   * {@inheritdoc}
   */
  protected function getRowWidths(): array {
    return [
      ZigguratLayout::ROW_WIDTH_25 => $this->t('25%'),
      ZigguratLayout::ROW_WIDTH_33 => $this->t('33%'),
      ZigguratLayout::ROW_WIDTH_50 => $this->t('50%'),
      ZigguratLayout::ROW_WIDTH_67 => $this->t('67%'),
      ZigguratLayout::ROW_WIDTH_75 => $this->t('75%'),
      ZigguratLayout::ROW_WIDTH_100 => $this->t('100%'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultRowWidth(): string {
    return ZigguratLayout::ROW_WIDTH_100;
  }

}
