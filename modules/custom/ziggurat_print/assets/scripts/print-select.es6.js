(($, Drupal) => {
  Drupal.behaviors.zigguratPrintSelect = {
    attach() {
      const queryStrings = window.location.href
        .slice(window.location.href.indexOf("?") + 1)
        .split("&");
      if (queryStrings.length === 0) {
        return;
      }
      let currentSelection = null;
      queryStrings.forEach(value => {
        const queryString = value.split("=");
        if (queryString.length !== 2) {
          return;
        }
        const [paramKey, paramValue] = queryString;
        if (paramKey === "current") {
          currentSelection = paramValue;
        }
      });
      if (!currentSelection) {
        return;
      }
      const $itemTitle = $(`span[data-print-selection="${currentSelection}"]`);
      if ($itemTitle.length === 0) {
        return;
      }
      $itemTitle
        .parents("tr")
        .find("input[type='checkbox']")
        .once("zigguratPrintSelect")
        .prop("checked", true);
    }
  };
})(jQuery, Drupal);
