"use strict";

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

(function ($, Drupal) {
  Drupal.behaviors.zigguratPrintSelect = {
    attach: function attach() {
      var queryStrings = window.location.href.slice(window.location.href.indexOf("?") + 1).split("&");
      if (queryStrings.length === 0) {
        return;
      }
      var currentSelection = null;
      queryStrings.forEach(function (value) {
        var queryString = value.split("=");
        if (queryString.length !== 2) {
          return;
        }

        var _queryString = _slicedToArray(queryString, 2),
            paramKey = _queryString[0],
            paramValue = _queryString[1];

        if (paramKey === "current") {
          currentSelection = paramValue;
        }
      });
      if (!currentSelection) {
        return;
      }
      var $itemTitle = $("span[data-print-selection=\"" + currentSelection + "\"]");
      if ($itemTitle.length === 0) {
        return;
      }
      $itemTitle.parents("tr").find("input[type='checkbox']").once("zigguratPrintSelect").prop("checked", true);
    }
  };
})(jQuery, Drupal);