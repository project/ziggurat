<?php

namespace Drupal\ziggurat_print\Plugin\Action;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsMessageInterface;

/**
 * An action that redirects to the path set in the view plugin.
 *
 * This action will compile all selected entities and redirect to a view
 * and with the entity IDs in the url using the separator chosen.
 *
 * @Action(
 *   id = "ziggurat_print_views_bulk_operations",
 *   label = @Translation("Print Items"),
 *   type = "",
 *   confirm = FALSE,
 * )
 */
class PrintViewBulkOperationAction extends ViewsBulkOperationsActionBase implements ViewsBulkOperationsMessageInterface {

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    return $entity->id();
  }

  /**
   * {@inheritdoc}
   */
  public function setContext(array &$context) {
    $this->context['sandbox'] = &$context['sandbox'];
    $this->context['results'] = &$context['results'];
    foreach ($context as $key => $item) {
      if ($key === 'sandbox' || $key === 'results') {
        continue;
      }
      $this->context[$key] = $item;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function executeMultiple(array $objects) {
    $ids = [];
    foreach ($objects as $entity) {
      $ids[] = $this->execute($entity);
    }
    $url = $this->configuration['page_redirect'];
    $separator = $this->configuration['filter_separator'];
    $path = $url . '/' . implode($separator, $ids);
    $this->context['results']['redirect_url'] = Url::fromUri("internal:/$path");
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildPreConfigurationForm(array $form, array $values, FormStateInterface $form_state) {
    $form['page_redirect'] = [
      '#title' => $this->t('The page to redirect to'),
      '#type' => 'textfield',
      '#default_value' => isset($values['page_redirect']) ? $values['page_redirect'] : '',
    ];
    $form['filter_separator'] = [
      '#title' => $this->t('Contextual filter separator'),
      '#type' => 'textfield',
      '#default_value' => isset($values['filter_separator']) ? $values['filter_separator'] : '+',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function getFinishedMessage($details): ?TranslatableMarkup {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function getDrushFinishMessage($details): TranslatableMarkup {
    // This is the default drush messaging.
    return \Drupal::translation()->translate(
      'Action processing results: @results.',
      ['@results' => implode(', ', $details)]
    );
  }

}
