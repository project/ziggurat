<?php

declare(strict_types = 1);

namespace Drupal\ziggurat_permissions;

use Drupal\Core\Database\Connection;

/**
 * Contain methods for retrieving Ziggurat permissions database content.
 *
 * @package Drupal\ziggurat_permissions
 */
class PermissionsLookup {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * Check for edit access for publication nodes.
   *
   * @param string $user_id
   *   The user id of the editor or artist to check.
   * @param string $publication
   *   The nid of the Publication node linked to the Publication Page to check.
   *
   * @return bool
   *   Whether or not the user has editing rights for the node.
   */
  public function checkAccess(string $user_id, string $publication): bool {
    // Build query.
    $query = $this->database->select('nodeaccess', 'a');
    $query->addField('a', 'nid', 'eid');
    $query->addField('a', 'gid', 'pid');
    $query->addField('a', 'grant_update', 'grant_update');
    $query->condition('a.gid', $user_id, '=');
    $query->condition('a.nid', $publication, '=');
    $query->condition('a.grant_update', 1, '=');

    // Check to see if there are any results.
    if ($results = $query->execute()->fetchAll()) {
      return TRUE;
    }
    return FALSE;
  }

}
