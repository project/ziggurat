<?php

declare(strict_types = 1);

namespace Drupal\ziggurat_permissions\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class ZigguratPermissionsRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    if ($route = $collection->get('entity.node.edit_form')) {
      $route->setRequirement('_ziggurat_editor_access_check', 'TRUE');
    }
  }

}
