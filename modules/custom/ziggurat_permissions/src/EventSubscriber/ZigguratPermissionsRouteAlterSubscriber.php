<?php

declare(strict_types = 1);

namespace Drupal\ziggurat_permissions\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Routing\RoutingEvents;
use Drupal\Core\Routing\RouteBuildEvent;

/**
 * Changes routing events to set different permissions for users.
 */
class ZigguratPermissionsRouteAlterSubscriber implements EventSubscriberInterface {

  /**
   * Alters the permissions check for layout builder node override access.
   *
   * @param \Drupal\Core\Routing\RouteBuildEvent $event
   *   The event to process.
   */
  public function onRoutingRouteAlterLayout(RouteBuildEvent $event): void {
    $collection = $event->getRouteCollection();
    if ($route = $collection->get('layout_builder.overrides.node.view')) {
      $route->setRequirement('_ziggurat_editor_access_check', 'TRUE');
    }
  }

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen to with weight alteration.
   */
  public static function getSubscribedEvents(): array {
    $events[RoutingEvents::ALTER][] = ['onRoutingRouteAlterLayout', -130];
    return $events;
  }

}
