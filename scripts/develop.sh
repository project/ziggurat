#!/usr/bin/env bash

# Fail safely if any errors occur.
set -eo pipefail

# Move up a level starting from the scripts directory.
BASE_DIR="$(dirname $(cd ${0%/*} && pwd))"
APP_NAME="develop-ziggurat"

COMPOSER="$(which composer)"
COMPOSER_BIN_DIR="$(composer config bin-dir)"
DOCROOT="web"

# Whether the source directory should be deleted before rebuilding lando
DELETE_SRC=0
REBUILD_ONLY=0
ZIGGURAT_VERSION="*"

while getopts o:drv: flag
do
    case "${flag}" in
	    d)  DELETE_SRC=1 ;;
	    o)  DEST_DIR=$OPTARG ;;
	    r)  REBUILD_ONLY=1 ;;
	    v)  ZIGGURAT_VERSION=$OPTARG ;;
    esac
done

if [ "$REBUILD_ONLY" == "1" ]  && [ "$DELETE_SRC" == "1" ];
then
  echo "Rebuild only and Delete Source options cannot be used together"
  exit
fi

# Make sure the system has lando.
if ! [ -x "$(command -v lando)" ]; then
  echo 'Error: Lando is not installed!' >&2
  exit 1
fi

LANDO="$(which lando)"

# Define the color scheme.
FG_C='\033[1;37m'
BG_C='\033[42m'
WBG_C='\033[43m'
EBG_C='\033[41m'
NO_C='\033[0m'

echo -e "\n"
if [ ! "$DEST_DIR" ] ; then
  DEST_DIR="$( dirname $BASE_DIR )/$APP_NAME"
  echo -e "${FG_C}${WBG_C} WARNING ${NO_C} No installation path provided.\Ziggurat Distribution will be installed in $DEST_DIR."
  echo -e "${FG_C}${BG_C} USAGE ${NO_C} ${0} [install_path] # to install in a different directory."
fi
DRUSH="$DEST_DIR/$COMPOSER_BIN_DIR/drush"

echo -e "\n\n\n"
echo -e "***************************************"
echo -e "*   Installing Ziggurat Distribution  *"
echo -e "***************************************"
echo -e "\n\n\n"
echo -e "Installing to: $DEST_DIR\n"

if [ -d "$DEST_DIR" ] && [ "$DELETE_SRC" == "1" ]; then
  set +e
  echo -e "${FG_C}${WBG_C} WARNING ${NO_C} You are about to delete $DEST_DIR to install Ziggurat Distribution in that location."

  # Destroy the lando containers before deletion.
  cd $DEST_DIR
  lando destroy --yes
  cd $BASE_DIR

  rm -Rf $DEST_DIR

  if [ $? -ne 0 ]; then
    echo -e "${FG_C}${EBG_C} ERROR ${NO_C} Sometimes drush adds some files with permissions that are not deletable by the current user."
    echo -e "${FG_C}${BG_C} EXECUTING ${NO_C} sudo rm -Rf $DEST_DIR"
    sudo rm -Rf $DEST_DIR
  fi
  set -e
fi

# Make sure the directory doesn't exist before making a new project.
if [ ! -d "$DEST_DIR" ]; then
  echo "-----------------------------------------------"
  echo " Setup Ziggurat Distribution using composer "
  echo "-----------------------------------------------"
  echo -e "${FG_C}${BG_C} EXECUTING ${NO_C} $COMPOSER create-project --no-interaction oomphinc/drupal-scaffold:1.1.2 ${DEST_DIR} --stability dev --no-interaction --no-install\n\n"
  $COMPOSER create-project --no-interaction oomphinc/drupal-scaffold:1.1.2 ${DEST_DIR} --stability dev --no-interaction --no-install
  if [ $? -ne 0 ]; then
    echo -e "${FG_C}${EBG_C} ERROR ${NO_C} There was a problem setting up Ziggurat Distribution using composer."
    echo "Please check your composer configuration and try again."
    exit 2
  fi
fi

echo "----------------------------------"
echo " Initialize lando for local usage "
echo "----------------------------------"
cd ${DEST_DIR}

if [ "$REBUILD_ONLY" == "0" ];
then
  echo -e "${FG_C}${BG_C} EXECUTING ${NO_C} $LANDO init --name $APP_NAME --recipe drupal8 --webroot web --source cwd\n\n"
  "$LANDO" init --name ${APP_NAME} --recipe drupal8 --webroot web --source cwd

  # Check for a lando local file.
  if [ -a ".lando.local.yml" ]; then
    # Using an if/else block as the negate (!) operator
    # doesn't seem to work with -a.
    echo "Found a .lando.local.yml file."
  else
    echo "Create a .lando.local.yml file."
    OUT=.lando.local.yml

    # Include a .lando.local.yml to include the distribution in the app.
    SERVICES=$(cat <<- EOF
  services:
    appserver:
      overrides:
        volumes:
          - $BASE_DIR:/ziggurat
    nodejs:
      overrides:
        volumes:
          - $BASE_DIR:/ziggurat
  config:
    composer_version: 1
    xdebug: true
EOF
  )
    echo "$SERVICES" > "$OUT"
  fi

  # Start the app with lando.
  "$LANDO" start

else
  # revert patches to nodeaccess module
  if [ -d "web/modules/contrib/nodeaccess" ]; then
    cd web/modules/contrib/nodeaccess
    git checkout .
    cd ${DEST_DIR}
  fi

  if [ -d "web/profiles/contrib/ziggurat" ]; then
    cd web/profiles/contrib/ziggurat
    git checkout .
    cd ${DEST_DIR}
  fi

  echo "--------------------------------------------------"
  echo "Removing previous package using $LANDO composer remove drupal/ziggurat"
  echo "--------------------------------------------------"
  "$LANDO" composer remove "drupal/ziggurat"
fi

echo "--------------------------------------------------"
echo " Require ziggurat using lando composer "
echo "--------------------------------------------------"
echo -e "${FG_C}${BG_C} EXECUTING ${NO_C} $LANDO composer config repositories.ziggurat url https://git.drupalcode.org/project/ziggurat\n\n"
echo -e "${FG_C}${BG_C} EXECUTING ${NO_C} $LANDO composer require 'drupal/ziggurat:$ZIGGURAT_VERSION' --no-progress\n\n"

"$LANDO" composer config repositories.ziggurat '{"type": "vcs", "url": "https://git.drupalcode.org/project/ziggurat"}'
"$LANDO" composer config extra.enable-patching true
"$LANDO" composer require "drupal/ziggurat:$ZIGGURAT_VERSION" --no-progress
"$LANDO" composer update --lock

# Install ziggurat profile.
cd ${DOCROOT}

# Ensure the sites/default directory is writeable.
if [ -d "${DEST_DIR}/web/sites/default/files" ]; then
  chmod ug+w ${DEST_DIR}/web/sites/default/files
fi

echo -e "${FG_C}${BG_C} EXECUTING ${NO_C} $LANDO drush site-install ziggurat"
"$LANDO" drush site-install ziggurat --verbose --yes --site-mail=admin@localhost --account-mail=admin@localhost --site-name='Ziggurat Distribution' --account-name=admin --account-pass=admin;

echo -e "${FG_C}${BG_C} EXECUTING ${NO_C} $LANDO drush updb --yes"
"$LANDO" drush updb --yes

echo -e "${FG_C}${BG_C} EXECUTING ${NO_C} $LANDO drush cr --yes"
"$LANDO" drush cr --yes

cd ${DEST_DIR}
"$LANDO" drush en -y ziggurat_default_content
"$LANDO" drush en -y nodeaccess

echo -e "\n\n\n"
echo -e "********************************"
echo -e "*    Installation finished     *"
echo -e "********************************"
echo -e "\n\n\n"
